## SER 215 - Pac-man

**How to Build**

* You must be using Java 1.8 or later
* In your IDE (We used IntelliJ IDEA), set the Source path to Pacman/src
* Set the Resource Path to Pacman/res
* Add lib/gson-2.8.0.jar as an external library to your module
* Run the project with Pacman/Core/Game as the entry point of the application

**Design Documentation**

* Game should have a main screen with the following

    * Logo of the game

    * Single player button to start the game

        * If difficulty modes are implemented, should lead to the selection of the difficulty

    * Credits button to view the authors

    * High scores shows the top 5 scores (if implemented)

    * Exit

* Player plays as Pac-man and moves around a maze collecting dots. Should have 3 lives. Dying respawns the player but does not change the maze and all objects persist while player spawns.

* Ghosts chase the player through the maze, collision with the player results in a kill

* The game should keep track of the score of the player point values below

<table>
  <tr>
    <td>Name</td>
    <td>Points</td>
  </tr>
  <tr>
    <td>Pac-dot</td>
    <td>10</td>
  </tr>
  <tr>
    <td>Power Pellet (Allows ghost kills)</td>
    <td>50</td>
  </tr>
  <tr>
    <td>Vulnerable Ghost</td>
    <td>200 base, increases by 2x if another ghost is killed within 5 seconds for a max of 1600</td>
  </tr>
  <tr>
    <td>Fruit</td>
    <td>Cherry - 100
Strawberry - 300
Orange - 500
Apple - 700
Melon - 1000</td>
  </tr>
</table>


* The maze should contain Power Pellets allowing Pac-man to kill the ghosts

* Game ends when player eats all Pac-dots or when they run out of lives.

* (Optional) Game should keep track of high scores

* (Optional) Game should have difficulty levels which could increase AI intelligence or make the maze larger, add a timer etc.

* (Optional) Mazes should be dynamically generated so they are different each time. Maze generation algorithm with a spanning tree algorithm to ensure no dead ends exist.

**Data Structures and Classes**

Actor - Any player or enemy object in the game. All classes that represent a game object should extend this base class. Should have a flag if the object can be moved (use speed = 0 and return a Boolean). Should have the following properties:

* Point (x and y coordinates on the JFrame it will be rendered on)

* Height and Width (will be used for the collision box, should match the sprite size)

* Bounding Box (calculate it one time from the Height and Width)

* Speed (the speed at which this actor can move, 0 if none movable). Speed should be modifiable during execution since Ghost speed is reduced when Power Pellets are in effect.

* Location relative to the map, index of where it is in the 2x2 array in the map

* Active State (should be the current resource index from the state table)

* States (should be the sprite or image to render), Some actors will need multiple resource states. Pacman and Ghost for example one for each direction they are facing. We should probably use a Hash for this or an array of a custom resource object. Then when we make a change call setState(str) where str is the index of the HashMap. 

* Blocking (should designate, that moving objects cannot intersect or move into this object)

Classes extending Actor:

* Pacman - Extends the Actor class and represents Pac-man giving the player the ability to move around, eat Pac-dots, fruit etc.

* Ghost - Extends the Actor class and represents an enemy. Should have some AI to move in the general direction of Pacman. For example, get the coordinates of the player and attempt to move in that direction, if there is a wall take a detour at forks pick a random direction. When a Power Pellet is active, the ghost should change sprite and 

* Item - Extends the Actor class and should grant the player points if the player collides with it. Remove it from the map if so. Only static non moving items should extend this class. Really we only need to extend it and change the value and resource in the new class.

* Wall - Represents a wall which moving objects cannot move into. Should have an orientation property and override the render method to rotate the resource vertically or horizontally.

Engine - Core game engine. Should use a game loop and cap the framerate at 60 FPS. Needs to handle all calculations for new frame data, collision detection and rendering using Java Swing

Map - Handles map generation (if we go that route), we should use a 2x2 array of Actors. Moving objects need to check with the map first to make sure the direction they try to move is clear isBlocking() for example.

Game - Entry point of the application. Should hand control over to MainMenu.

MainMenu - Should handle the launching of the game engine and additional dialogs.