import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class MainForm extends JFrame implements KeyListener {
    private JButton btnBrowse;
    private JTextField txtMap;
    private JPanel panel;
    private JButton btnConvert;
    private JPanel panel2;
    private JPanel panel1;
    private JTextField txtTileset;
    private JButton btnBrowseTileset;

    private Map<String, String> tileset;
    private JsonMap map = new JsonMap();

    private File mapFile;

    public MainForm() {
        this.setTitle("Tiled to Map");
        this.add(panel);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        btnBrowse.addActionListener(MainForm.this::onBrowseClick);
        btnBrowseTileset.addActionListener(MainForm.this::onBrowseClick);
        btnConvert.addActionListener(MainForm.this::convert);

        this.addKeyListener(this);
        panel.addKeyListener(this);
        panel1.addKeyListener(this);
        panel2.addKeyListener(this);
        txtMap.addKeyListener(this);
        btnBrowse.addKeyListener(this);
        btnConvert.addKeyListener(this);
    }

    protected void onBrowseClick(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();

        if (e.getSource().equals(btnBrowse)) {
            fileChooser.setFileFilter(new FileNameExtensionFilter("CSV Files (*.csv)", "csv"));
        } else {
            fileChooser.setFileFilter(new FileNameExtensionFilter("JSON Files (*.json)", "json"));
        }

        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File f = fileChooser.getSelectedFile();
            if (!f.isFile() || !f.exists()) {
                JOptionPane.showMessageDialog(this, "Error! Selected file does not exist or is a directory.",
                        "Invalid selection", JOptionPane.ERROR_MESSAGE);
                return;
            }

            String path = f.getAbsolutePath();
            if (e.getSource().equals(btnBrowse)) {
                mapFile = f;
                txtMap.setText(path);
            } else {
                txtTileset.setText(path);
            }
        }
    }

    public void loadTileset() {
        if (txtTileset.getText().equals("")) {
            tileset = new HashMap<String, String>() {
                {
                    put("-1", "");
                    put("0", "wall");
                    put("1", "pdot");
                    put("2", "ppellet");
                    put("3", "teleport");
                    put("4", "pspawn");
                    put("5", "gspawn");
                    put("6", "owwall");
                }
            };
            return;
        }

        Gson gson = new Gson();
        try {
            java.lang.reflect.Type type = new TypeToken<Map<String, String>>(){}.getType();
            tileset = gson.fromJson(new FileReader(txtTileset.getText()), type);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error! Unable to read selected tileset file.",
                    "Unable to read file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void convert(ActionEvent e) {
        if (mapFile == null) {
            JOptionPane.showMessageDialog(this, "Error! No file selected. You must select a file to convert.", "No file selected",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Load up the file
        List<String[]> buffer = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(mapFile));
            String line;
            int x = 0, y = 0, last = -1;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(",");
                buffer.add(tokens);
                x = tokens.length;
                if (x != last && last != -1)
                    JOptionPane.showMessageDialog(this, "The map data is corrupted and does not contain the same number of " +
                            "columns in each row.\nError on line: " + (y + 1), "Corrupted data", JOptionPane.ERROR_MESSAGE);
                last = x;
                y++;
            }

            map.sizeX = x;
            map.sizeY = y;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error! Unable to open the selected file.",
                    "Unable to read file", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // If we made it this far, the file loaded
        // parse it
        parse(buffer.toArray(new String[0][0]));
    }

    private void parse(String[][] buffer) {
        // Sanity check!
        if (map.sizeX <= 0 && map.sizeY <= 0 ) {
            JOptionPane.showMessageDialog(this, "Warning! Map was empty or we were unable to load data.",
                    "Empty map", JOptionPane.WARNING_MESSAGE);
            return;
        }

        loadTileset();
        if (tileset == null) {
            JOptionPane.showMessageDialog(this, "Error! Tileset is empty. Conversion halted",
                    "Tileset not loaded", JOptionPane.ERROR_MESSAGE);
        }

        map.init();
        for (int y = 0; y < map.sizeY; y++) {
            for (int x = 0; x < map.sizeX; x++) {
                map.actors[y][x] = tileset.get(buffer[y][x]);
            }
        }

        // Get the name of the file with extension
        String name = mapFile.getName();
        int pos = name.lastIndexOf(".");
        if (pos > 0) {
            name = name.substring(0, pos);
        }
        // Get the output path from the original file
        File output = new File(mapFile.getParent(), name.concat(".json"));
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(map);
        try {
            FileWriter writer = new FileWriter(output);
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error! Unable to write to " + output.getAbsolutePath() + ". " + e.getMessage(),
                    "Unable to write to file", JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(this, "Conversion complete!", "Done", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override public void keyTyped(KeyEvent e) { }
    @Override public void keyReleased(KeyEvent e) { }
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
            dispatchEvent(new WindowEvent(MainForm.this, WindowEvent.WINDOW_CLOSING));
    }
}
