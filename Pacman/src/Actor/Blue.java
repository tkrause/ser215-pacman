package Actor;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.HashSet;

import Actor.Ghost_AI;
import Core.Map;
import Engine.PrecisePoint;
import Engine.State;



public class Blue extends Ghost_AI {
	protected static java.util.Map<String, State> states = new HashMap<>();

	@Override
	public java.util.Map<String, State> getStates() {
		return states;
	}

	@Override
	public Point destination() {
		Map map = Map.make();
		return map.getCurrentSquare(Pacman.make());
		//red always chases Pacman
	}

	@Override
	public void init() {
		this.addState("right", "/Actor/Inky/right.gif");
		this.addState("left", "/Actor/Inky/left.gif");
		this.addState("up", "/Actor/Inky/up.gif");
		this.addState("down", "/Actor/Inky/down.gif");
		this.addState("vulnerable", "/Actor/ghost-vulnerable.gif");
		this.setActiveState("right");
		this.setHandlesCollision(true);
		this.setSpeed(108);
	}
}
