package Actor.Item;

import Engine.State;

import java.util.HashMap;
import java.util.Map;

public class PowerPellet extends Item {
    protected static Map<String, State> states = new HashMap<>();

    @Override
    public void init() {
        this.addState("idle", "/Actor/Item/power-pellet.gif");
    }

    @Override
    public Map<String, State> getStates() {
        return states;
    }
}
