package Actor.Item;

import Engine.Actor;
import Engine.Engine;

import java.awt.event.KeyEvent;
import java.util.List;

public abstract class Item extends Actor {
    public Item() {
        super(24, 24);
    }

    public Item(int height, int width) {
        super(height, width);
    }

    @Override public void input(List<KeyEvent> keys, double delta) { }
    @Override public void tick(double delta) { }
    @Override public void collide(Actor actor, boolean simulated) { }

    public void kill() {
        Engine.make().deleteActor(this);
    }
    public static void score() {
		
	}
}
