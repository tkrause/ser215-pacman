package Actor.Item;

import Engine.State;

import java.util.HashMap;
import java.util.Map;

public class Pacdot extends Item {
    protected static Map<String, State> states = new HashMap<>();

    public Pacdot() {
        super(13, 13);
    }

    @Override
    public void init() {
        this.addState("idle", "/Actor/Item/pacdot.gif");
    }

    @Override
    public Map<String, State> getStates() {
        return states;
    }
}
