package Core;

import Actor.Pacman;
import Actor.ScoreBoard;
import Engine.Engine;
import UI.GUI;

import java.io.IOException;

public class Game {
    public static void main(String[] args) {
        openMenu();
    }

    public static void launch() {
        Engine.make()
              //.setDebug(true)
              .setOnDestroy(() -> {
                  Map.destroy();
                  Pacman.destroy();
                  ScoreBoard.destroy();
              })
              .setOnEscape(() -> {
                  Engine.make().stop();
                  openMenu();
              });

        Map.make();
        Engine.make().start();
    }

    public static void gameOver(int score) {
        try {
            Engine.make().stop();
            GameOver.gameOver(score);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openMenu() {
        new GUI();
    }
}
