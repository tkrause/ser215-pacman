package Core;

import java.awt.*;
import java.io.IOException;
import javax.swing.*;
public class GameOver extends JFrame
{
    public static void gameOver (int score) throws IOException
    {
    	//Specify the path of the gameOver of jpg.
        JLabel label = new JLabel(new ImageIcon(GameOver.class.getResource("/gameOver.jpg")));

        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(label);
        f.pack();
        f.setLocation(200,200);
        f.setVisible(true);
        f.setLocationRelativeTo(null);

        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        f.setVisible(false); 
        f.dispose(); //Destroy the JFrame object and essentially closes game.
        Game.openMenu(); // Launches gome again.
    }
}