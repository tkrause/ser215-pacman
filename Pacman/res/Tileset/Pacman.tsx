<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Pacman" tilewidth="45" tileheight="45" tilecount="7" columns="0">
 <tile id="0">
  <image width="45" height="45" source="../Actor/wall.gif"/>
 </tile>
 <tile id="1">
  <image width="45" height="45" source="../Actor/item/pacdot.gif"/>
 </tile>
 <tile id="2">
  <image width="45" height="45" source="../Actor/Item/power-pellet.gif"/>
 </tile>
 <tile id="3">
  <image width="45" height="45" source="teleporter.gif"/>
 </tile>
 <tile id="4">
  <image width="45" height="45" source="player-spawn.gif"/>
 </tile>
 <tile id="5">
  <image width="45" height="45" source="ghost-spawn.gif"/>
 </tile>
 <tile id="6">
  <image width="45" height="45" source="../Actor/wall-one-way.gif"/>
 </tile>
</tileset>
